package day3;
import java.util.Scanner;
public class loop2 {

	public static void main(String[] args) {
		Scanner masukan = new Scanner(System.in);
		System.out.print("Masukan: ");
		int n = masukan.nextInt();
		
		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {
			
				if (i == j || i+j == n-1  ) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				} 
			}
			System.out.println();
		}
		
	}

}