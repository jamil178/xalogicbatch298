package TugasWeekend;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class soalbaru {

	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {

				System.out.print("Masukan Soal: ");
				int number = in.nextInt();
				in.nextLine();
				switch (number) {
				case 1:
					soal1();

					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();

					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();

					break;
				case 6:
					soal6();

					break;
				case 7:
					soal7();

					break;
				case 8:
					soal8();

					break;
				default:
					System.out.println("Sorry Gak Ada Yang DI Pilih");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Mau Masukan Soal?");
			answer = in.next();
		}

	}

	private static void soal1() {
		System.out.println();
		System.out.println("-----WELCOME TO TOGEL ABAH ZEUS------");
		System.out.println();
		System.out.print("Masukan Point= ");
		int point = in.nextInt();
		String jawab = "Y";
		while (jawab.toUpperCase().equals("Y")) {
			System.out.println();
			System.out.print("Masukan Taruhan= ");
			int taruhan = in.nextInt();
			if (taruhan > point) {
				System.out.println("Maaf Taruhan Mu Kelebihan Bro");
				break;
			}
			System.out.print("Tebak U/D= ");
			String tebak = in.next();
			int nilai = (int) (Math.random() * 10);
			System.out.println("Angka Muncul= " + nilai);
			if (tebak.equals("u") && nilai > 5) {
				System.out.println("You Win");
				point = point + taruhan;
			} else if (tebak.equals("d") && nilai < 5) {
				System.out.println("You Win");
				point = point + taruhan;
			} else if (nilai == 5) {
				System.out.println("Draw");
			} else {
				System.out.println("You Lose");
				point = point - taruhan;

			}
			System.out.println();
			System.out.println("Point Kamu: " + point);
			if (point == 0) {
				System.out.println("Game Over");
				break;
			} else {
				System.out.print("Mau Ulang: ");
				jawab = in.next().toLowerCase();
			}

		}

	}

	private static void soal2() {
		System.out.print("Masukan Jumlah Keranjang: ");
		int keranjang = in.nextInt();
		String[] buah = new String[keranjang];
		System.out.print("Masukan index Keranjang yang dibawa: ");
		String index = in.next();
		String[] split = index.split(",");
//		in.nextLine();
		int k = 0;
		for (int i = 0; i < keranjang; i++) {

			System.out.print("Masukan Buah di Keranjang " + (i + 1) + ": ");
			buah[i] = in.next();

			if (buah[i].equals("kosong")) {
				buah[i] = "0";
			}

			k = k + Integer.parseInt(buah[i]);

		}
		for(int i=0; i<split.length; i++) {
			int nilai = Integer.parseInt(split[i])-1;
			k=k-Integer.parseInt(buah[nilai]); 
		}
			
		System.out.println("Sisa Buah adalah : " + k);
	}

	private static void soal3() {
		System.out.println("Masukan X: ");
		int x = in.nextInt();
		int k = x;
		for (int i = 1; i < x; i++) {
			k = k * i;
			System.out.println(k);
		}

		System.out.println("Ada " + k + " Cara");

	}

	private static void soal4() {
		int orang[] = new int[4];
		System.out.print("Masukan Jumlah Laki-Laki Dewasa: ");
		orang[0] = in.nextInt() * 1;
		System.out.print("Masukan Jumlah Wanita Dewasa: ");
		orang[1] = in.nextInt() * 2;
		System.out.print("Masukan Jumlah Anak : ");
		orang[2] = in.nextInt() * 3;
		System.out.print("Masukan Jumlah Bayi : ");
		orang[3] = in.nextInt() * 5;

		int baju = 0;
		for (int i = 0; i < orang.length; i++) {
			baju += orang[i];
		}
		if (baju % 2 == 1 && baju >= 10) {
			baju = (orang[1] / 2) + baju;
			System.out.println("Wanita Dewasa  Dapat : 1 Baju");
			System.out.println("Jumlah Baju: " + baju);
		} else {
			System.out.println("Jumlah Baju = " + baju);
		}
	}

	private static void soal5() {
		System.out.print("Masukan Jumlah Kue Pukis: ");
		int pukis = in.nextInt();
		if(pukis>0) {
		double tepung = pukis * 8.33;
		double gula = pukis * 6.66;
		double susu = pukis * 6.66;
		double telur = pukis * 6.66;
		DecimalFormat NumberFormat = new DecimalFormat("0.00");
		System.out.println("Tepung Terigu= " +NumberFormat.format(tepung) + " Gram");
		System.out.println("Gula Pasir= "  +NumberFormat.format(gula) + " Gram");
		System.out.println("Susu Murni= " + NumberFormat.format(susu) + " Gram");
		System.out.println("Putih Telur= " + NumberFormat.format(telur) + " Gram");
		}
		else {
			System.out.println("Nilai Tidak Sesuai");
		}
	}

	private static void soal6() throws ParseException {

		System.out.print("Waktu Masuk (dd/MM/yyyy HH.mm): ");
		String jammasuk = in.nextLine();
		System.out.print("Waktu Keluar (dd/MM/yyyy HH.mm): ");
		String jamkeluar = in.nextLine();

		String[] jam = new String[2];
		jam[0] = jammasuk;
		jam[1] = jamkeluar;

		for (int i = 0; i < jam.length; i++) {
			for (int j = 0; j < jam[i].length(); j++) {

			}
			

			Date waktumasuk = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(jammasuk);
			Date waktukeluar = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(jamkeluar);

			long compareInMiliSecond = waktukeluar.getTime() - waktumasuk.getTime();
			double compareInHours = compareInMiliSecond / (3600 * 1000);
			double compareInHoursdb = Math.ceil((double) compareInHours);
			double compareInDay = compareInMiliSecond / (3600 * 1000 *24);
			double compareInDaysdb = Math.ceil((double) compareInDay);
			int harga=0;
			if(compareInHoursdb>=8) {
			harga = (int) compareInDaysdb * 15000;
			}
			else {
				harga = (int) compareInHoursdb * 3000;
			}
			System.out.println("Durasi parkir adalah " + (int) compareInHoursdb + " jam");
			System.out.println("Harga parkir yang harus dibayar adalah Rp." + harga);

		}
	}

	private static void soal7() throws ParseException {

		System.out.print("Silahkan masukkan waktu datang (dd-MM-yyyy): "); // 09-06-2019
		String tanggal1 = in.nextLine();
		System.out.print("Silahkan masukkan waktu pergi (dd-MM-yyyy): "); // 10-07-2019
		String tanggal2 = in.nextLine();

		String[] tanggal = new String[2];
		tanggal[0] = tanggal1;
		tanggal[1] = tanggal2;

		for (int i = 0; i < tanggal.length; i++) {
			for (int j = 0; j < tanggal[i].length(); j++) {

			}

			Date waktuPeminjaman = new SimpleDateFormat("dd-MM-yyyy").parse(tanggal1);
			Date waktuPengembalian = new SimpleDateFormat("dd-MM-yyyy").parse(tanggal2);

			long gapInMiliSecond = waktuPengembalian.getTime() - waktuPeminjaman.getTime();
			double gapInDays = gapInMiliSecond / (3600 * 1000 * 24);
			double gapInDaysDb = Math.ceil((double) gapInDays);
			if (gapInDaysDb > 3) {
				int denda = ((int) gapInDaysDb - 3) * 500;
				System.out.println("Lama keterlambatan pengembalian adalah " + ((int) gapInDaysDb - 3) + " hari");
				System.out.println("Jumlah denda yang harus dibayar adalah Rp." + denda);
				break;
			} else {
				System.out.println("Terimakasih karena anda telah mengembalikan buku sebelum terlambat");
			}
		}
	}

	private static void soal8() {
		System.out.println("Masukan Inputan: ");
		int inputan = in.nextInt();
		for (int l = 0; l < inputan; l++) {
			System.out.print("Masukkan kata: ");
			String kata = in.next().toLowerCase();
			char[] kata2 = kata.toCharArray();

			if (kata.equals("bakso")) {
				System.out.println("Bahe");
			}

			String k = "";
			for (int i = kata2.length - 1; i >= 0; i--) {
				k += kata2[i];
			}
			if (k.equals(kata)) {
				System.out.println("Yes");
			} else {
				System.out.println("No");
			}
		}
	}

}
