package day15;

import java.util.Scanner;

public class Main {

	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {

				System.out.print("Masukan Soal: ");
				int number = in.nextInt();
				in.nextLine();
				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;

				default:
					System.out.println("Sorry Gak Ada Yang DI Pilih");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Mau Masukan Soal?");
			answer = in.next();

		}
	}

	private static void soal1() {
		System.out.println("Masukan Nilai N: ");
		int n = in.nextInt();
		int hasil = 0;
		for (int i = 1; i <= n; i++) {

			hasil = i * 2 - 1;
			if (hasil <= n) {
				System.out.print(hasil + " ");

			}
		}
		System.out.println();
		for (int i = 1; i <= n; i++) {

			hasil = i * 2;
			if (hasil <= n) {
				System.out.print(hasil + " ");

			}
		}

	}

	private static void soal2() {
		System.out.println("Masukan Kalimat= ");
		String huruf = in.nextLine().toLowerCase();
		String charmasukan = huruf.replaceAll(" ", "");
		char[] arraymasukan = charmasukan.toCharArray();

		int hasil = 0;
		char vokal = 0;
		char konsonan = 0;
		for (int i = 0; i < arraymasukan.length; i++) {
			for (int j = 0; j < arraymasukan.length; j++) {

				if (arraymasukan[i] < arraymasukan[j]) {
					hasil = arraymasukan[i];
					arraymasukan[i] = arraymasukan[j];
					arraymasukan[j] = (char) hasil;

				}
			}
		}
		System.out.println();

//
//			for (int j = 0; j < arraymasukan.length; j++) {
//				if(arraymasukan[j]=='a'||arraymasukan[j]=='i'||arraymasukan[j]=='u'||arraymasukan[j]=='e'||arraymasukan[j]=='o')
//					System.out.println("huruf vokal: "+arraymasukan[j]);
//			}
		int j = 0;
		while (j < arraymasukan.length) {
			if (arraymasukan[j] == 'a' || arraymasukan[j] == 'i' || arraymasukan[j] == 'u' || arraymasukan[j] == 'e'
					|| arraymasukan[j] == 'o')

				System.out.println("vokal= " + arraymasukan[j]);
			else {
				System.out.println("konsonan= " + arraymasukan[j]);
			}
			j++;
		}
	}

//			System.out.println();
//			for (int i = 0; i < arraymasukan.length; i++) {
//				if(arraymasukan[i]!='a'||arraymasukan[i]!='i'||arraymasukan[i]!='u'||arraymasukan[i]!='e'||arraymasukan[i]!='o')
//				
//					System.out.println("huruf konsonan: "+arraymasukan[i]);
//			}
//			
//		}
	private static void soal3() {
		System.out.println("Masukan Nilai N= ");
		int n = in.nextInt();
		int angka = 0;
		for (int i = 0; i < n; i++) {
			angka = 100 + (n * i);
			System.out.println(angka + " Si Angka 1");
		}
	}

	private static void soal4() {
		System.out.print("Masukkan indeks kostemer: ");
		String inputKostemer = in.nextLine();
		int[] arrayJarakKostemer = { 2000, 500, 1500, 300 };
		int bensin = 2500;
		String tempString = "";
		int tempInt = 0;
		String[] arrayInputKostemer = inputKostemer.split(" ");
		if (arrayInputKostemer.length > arrayJarakKostemer.length) {
			System.out.println("Input melebihi database");
			return;
		}
		if (!arrayInputKostemer[0].equals("1")) {
			System.out.println("Maaf orderan salah");
			return;
		}
		for (int i = 0; i < arrayInputKostemer.length; i++) {
			if (Integer.parseInt(arrayInputKostemer[i]) > tempInt) {
				tempInt = Integer.parseInt(arrayInputKostemer[i]);
			}
		}
		if (tempInt > arrayJarakKostemer.length) {
			System.out.println("Indeks maksimal input melebihi database");
			return;
		}
		for (int i = 0; i < tempInt; i++) {
			tempString += (i + 1) + "";
			if (i != tempInt - 1) { // tambah spasi
				tempString += " ";
			}
		}
		arrayInputKostemer = tempString.split(" ");
		tempString = "";
		tempInt = 0;
		for (int i = 0; i < arrayInputKostemer.length; i++) {
			int kostemer = Integer.parseInt(arrayInputKostemer[i]) - 1;

			String jarakKostemer = "";
			if (arrayJarakKostemer[kostemer] <= 1000) {
				jarakKostemer = arrayJarakKostemer[kostemer] + "M";
			} else {
				jarakKostemer = (arrayJarakKostemer[kostemer] / 1000.0) + "KM";
			}

			if (i == 0) {
				tempString += "Jarak tempuh = " + jarakKostemer + " + ";
				tempInt += arrayJarakKostemer[kostemer];
			} else if (i == arrayInputKostemer.length - 1) {
				tempString += jarakKostemer;
				tempInt += arrayJarakKostemer[kostemer];
			} else {
				tempString += jarakKostemer + " + ";
				tempInt += arrayJarakKostemer[kostemer];
			}
		}
		// perhitungan hasil jarak
		if (tempInt <= 1000) {
			tempString += " = " + tempInt + "M" + "\n";
		} else {
			tempString += " = " + (tempInt / 1000.0) + "KM" + "\n";
		}
		tempInt /= bensin;
		if (tempInt % bensin != 0) {
			tempInt++;
		}
		tempString += "Bensin = " + tempInt + " Liter";
		tempString = tempString.replaceAll(".0K", "K");

		System.out.println(tempString);

	}

	private static void soal5() {
		double orang[] = new double[5];
		System.out.print("Masukan Jumlah Laki-Laki Dewasa: ");
		orang[0] = in.nextInt() * 2;
		System.out.print("Masukan Jumlah Wanita Dewasa: ");
		orang[1] = in.nextInt() * 1;
		System.out.print("Masukan Jumlah Anak : ");
		orang[2] = in.nextInt() * 0.5;
		System.out.print("Masukan Jumlah Bayi : ");
		orang[3] = in.nextInt() * 0.5;
		System.out.print("Masukan Jumlah Remaja : ");
		orang[4] = in.nextInt() * 1;

		double makan = 0;
		for (int i = 0; i < orang.length; i++) {
			makan += orang[i];
		}
		if (makan % 2 == 1 && makan >= 5) {
			makan = (int) ((orang[1] + (orang[1 * 1])) + makan);
			System.out.println("Wanita Dewasa  Dapat : 1 makan");
			System.out.println("Jumlah Makan: " + Math.ceil(makan));
		} else {
			System.out.println("Jumlah Makan = " + Math.ceil(makan));
		}
	}

	private static void soal6() {
			System.out.println("Masukan Pin= ");
			String pin = in.nextLine().toLowerCase();
			String charmasukan = pin.replaceAll(" ", "");
			char [] arraymasukan = charmasukan.toCharArray();
			if(arraymasukan.length>6||arraymasukan.length<6) {
				System.out.println("Pin kelebihan");
				return;
			}
		
			System.out.println("Masukan Uang Setor: ");
			long uang = in.nextInt();
			
		
				System.out.print("Masukan Bank: ");
				int pilihan = in.nextInt();
				if(pilihan==1) {
				System.out.println("Anda Memilih Antar Rekening");
				in.nextLine();
				System.out.println("Masukan Rekeninng Tujuan= ");
				String rekening = in.nextLine().toLowerCase();
				charmasukan = rekening.replaceAll(" ", "");
				arraymasukan= charmasukan.toCharArray();
				if(arraymasukan.length>10||arraymasukan.length<10) {
					System.out.println("Rekening Salah");
					return;
				}
				else {
					
				}
				
				System.out.println("Masukan Jumlah Transfer: ");
				long tf =in.nextLong();
				if(tf>uang) {
					System.out.println("Uang Anda Tidak Cukup");
				}
				else {
					double total = uang- tf;
					System.out.println();
					System.out.println("Sisa Saldo Rp."+total);
				}

				}
				if(pilihan==2) {
					System.out.println("Anda Memilih Antar Bank");
					in.nextLine();
					System.out.println("Masukan Rekeninng Tujuan= ");
					String rekening = in.nextLine().toLowerCase();
					charmasukan = rekening.replaceAll(" ", "");
					arraymasukan= charmasukan.toCharArray();
					if(arraymasukan.length>10||arraymasukan.length<10) {
						System.out.println("Rekening Salah");
						return;
					}
					else {
						
					}
					
					System.out.println("Masukan Jumlah Transfer: ");
					long tf =in.nextLong();
					if(tf>uang) {
						System.out.println("Uang Anda Tidak Cukup");
					}
					else {
						double total = uang- tf - 7500;
						System.out.println();
						System.out.println("Sisa Saldo Rp."+total);
					}

					}
				
	
	
			}
	
	
	
	
	
	private static void soal7() {
		System.out.println();
		System.out.print("Masukan Jumlah Kartu= ");
		int point = in.nextInt();
		String jawab = "Y";
		while (jawab.toUpperCase().equals("Y")) {
			System.out.println();
			System.out.print("Masukan Taruhan= ");
			int taruhan = in.nextInt();
			if (taruhan > point) {
				System.out.println("Maaf Taruhan Mu Kelebihan Bro");
				break;
			}
			
			
			
			
			System.out.print("Tebak A/B= ");
			String tebak = in.next();
			int nilaia = (int) (Math.random() * 10);
			int nilaib = (int) (Math.random() * 10);
			int a=nilaia;
			int b=nilaib;
			if (tebak.equals("A") && a > b) {
				System.out.println("Nilai Kotak A: "+nilaia);
				System.out.println("Nilai Kotak B: "+nilaib);
				System.out.println("You Win");
				point = point + taruhan;
			} else if (tebak.equals("B") && b > a) {
				System.out.println("Nilai Kotak A: "+nilaia);
				System.out.println("Nilai Kotak B: "+nilaib);
				System.out.println("You Win");
				point = point + taruhan;
			} else if (tebak.equals("B") && a == b) {
				System.out.println("Nilai Kotak A: "+nilaia);
				System.out.println("Nilai Kotak B: "+nilaib);
				System.out.println("DRAW");
			
			} else if (tebak.equals("A") && a == b) {
				System.out.println("Nilai Kotak A: "+nilaia);
				System.out.println("Nilai Kotak B: "+nilaib);
				System.out.println("DRAW");
			
				
			} else {
				System.out.println("Nilai Kotak A: "+nilaia);
				System.out.println("Nilai Kotak B: "+nilaib);
				System.out.println("You Lose");
				point = point - taruhan;

			}
			System.out.println();
			System.out.println("Kartu Kamu: " + point);
			if (point == 0) {
				System.out.println("Game Over");
				break;
			} else {
				System.out.print("Mau Ulang: ");
				jawab = in.next().toLowerCase();
			}

		} //perbaharui 

	}
		
		
	

	private static void soal8() {
		
	
		
		
	}

	private static void soal9() {
		System.out.println("Masukan Perjalanan Hatori : "); 
		String huruf = in.nextLine().toUpperCase();
		String charmasuk = huruf.replaceAll(" ", "");
		char[] arraymasukan = charmasuk.toCharArray();
		
		if(arraymasukan.length=='N' || arraymasukan.length=='T') {
			System.out.println("Inputan Salah");
			return;
		}
		
		for (int i = 0; i < arraymasukan.length; i++)
			for (int j = 0; j < arraymasukan.length; j++)

				if (arraymasukan[i] < arraymasukan[j])

				{

					int hasil = arraymasukan[i];

					arraymasukan[i] = arraymasukan[j];

					arraymasukan[j] = (char) hasil;
					
					System.out.println(hasil);

				}
		int angka=0;

		System.out.println();

		for (int j = 0; j < arraymasukan.length; j++) {
			if (j == 0) {
				System.out.print(arraymasukan[j] + "");
			}
			
			else if (arraymasukan[j] != arraymasukan[j-1] && j !=0) {
				System.out.print(" - "+ arraymasukan[j]);
				angka=arraymasukan[j];
				
			}
			else {
				System.out.print(arraymasukan[j]);
				
			}
		}
	
		
		
		
	}
	
	

		
	

	private static void soal10() {
	
	
	
	
	}
	
	
}