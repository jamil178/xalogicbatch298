package day5;

public class StringManupulation {

	public static void main(String[] args) {
		// Split eliminasi kata
		String s1 = "Aku Cinta Kamu";
		String[] words = s1.split(" ");
		System.out.println(words[2]);

		// eliminasi huruf
		char[] word2 = words[2].toCharArray();
		System.out.println(word2[word2.length - 1]);
		System.out.println(word2[2]);
		
		// mengeleminasi sebuah huruf
		//(perhatikan pembatasan harus lebih dari yang di mau)
		String word3 = s1.substring(6, 8);
		System.out.println(word3);

		// Replace Karakter
		String  word4 = s1.replaceAll(" ", "");
		System.out.println(word4);
		
		String word5 = s1.toUpperCase();
		word5 = word5.replace(" ", "");
		System.out.println(word5);
		
		String word = s1.toLowerCase();
		System.out.println(word);
	}
}
