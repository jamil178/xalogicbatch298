package day8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {

				System.out.print("Masukan Soal: ");
				int number = in.nextInt();
				in.nextLine();
				switch (number) {
				case 1:
					soal1();

					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();

					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();

					break;
				case 6:
					soal6();

					break;
				case 7:
					soal7();

					break;
				case 8:
					soal8();

					break;
				case 9:
					soal9();

					break;
				case 10:
					soal10();

					break;
				default:
					System.out.println("Sorry Gak Ada Yang DI Pilih");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Mau Masukan Soal?");
			answer = in.next();
		}

	}

	private static void soal1() {
		System.out.println("Masukan Kata: ");// kalimat pertama harus lower case
		String a = in.next();
		// kalimat seterusnya harus berawalan Uppercase
		char[] achar = a.toCharArray();
		int p = 0;

		for (int i = 0; i < achar.length; i++) {
			if (Character.isUpperCase(achar[i]) || i == 0) {
				p++;
			}

		}
		System.out.println(p);
	}

	private static void soal2() {
		System.out.print("Masukkan jumlah karakter Password: ");
		int jumlah = in.nextInt();
		in.nextLine();
		System.out.print("Masukkan Password: ");
		String password = in.next();
		char[] pass = password.toCharArray();
		if (jumlah != password.length()) {
			System.out.println("Maaf Karakter yang Anda Masukan Salah");
			return;
		}

		if (password.length() < 6) {
			System.out.println(6 - password.length());
			return;
		}

		String num = "0123456789";
		char[] number = num.toCharArray();
		String lc = "abcdefghijklmnopqrstuvwxyz";
		char[] lowCase = lc.toCharArray();
		String uc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		char[] upCase = uc.toCharArray();
		String sc = "!@#$%^&*()-+";
		char[] spChar = sc.toCharArray();

		int Number = 0;
		int Lowercase = 0;
		int Uppercase = 0;
		int Special = 0;

		for (int i = 0; i < pass.length; i++) {
			for (int j = 0; j < number.length; j++) {
				if (pass[i] == number[j]) {
					Number++;
				}
			}
			for (int k = 0; k < lowCase.length; k++) {
				if (pass[i] == lowCase[k]) {
					Lowercase++;
				}
			}
			for (int l = 0; l < upCase.length; l++) {
				if (pass[i] == upCase[l]) {
					Uppercase++;
				}
			}
			for (int m = 0; m < spChar.length; m++) {
				if (pass[i] == spChar[m]) {
					Special++;
				}
			}
		}

		if (Number == 0 || Lowercase == 0 || Uppercase == 0 || Special == 0) {
			System.out.println("1");
		}
	}

	private static void soal3() {

	}

	private static void soal4() {
		System.out.print("Masukkan SOS Anda: ");
		String sos = in.nextLine();
		char[] arrayCharSos = sos.toLowerCase().toCharArray();
		int k = 0;
		for (int i = 0; i < arrayCharSos.length; i++) {
			if (i % 3 == 0 && arrayCharSos[i] == 's') {
				continue;
			} else if (i % 3 == 1 && arrayCharSos[i] == 'o') {
				continue;
			} else if (i % 3 == 2 && arrayCharSos[i] == 's') {
				continue;
			} else {
				k++;
			}
		}

		System.out.println(k);
	}

	private static void soal5() {

		System.out.print("Masukkan Inputan: ");
		int jumlah = in.nextInt();

		String[] kata = new String[jumlah];
		String[] hasil = new String[jumlah];

		String huruf = "hackerrank";
		for (int i = 0; i < jumlah; i++) {
			System.out.print("Masukkan kata ke-" + (i + 1) + ": ");
			kata[i] = in.next().toLowerCase();
		}

		for (int j = 0; j < jumlah; j++) {
			int l = 0;
			hasil[j] = "";
			for (int k = 0; k < kata[j].length(); k++) {
				while (l < huruf.length() && kata[j].charAt(k) 
						== huruf.charAt(l)) {
					hasil[j] += huruf.charAt(l);
					l++;
				}
			}
			System.out.println(hasil[j]);

			if (hasil[j].equals(huruf)) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}

	}

	private static void soal6() {

		System.out.print("Masukkan Kalimat : ");
		String kalimat = in.nextLine().toLowerCase();
		String huruf = "abcdefghijklmnopqrstuvwxyz ";
		String hasil = "";

		for (int i = 0; i < huruf.length(); i++) {
			for (int j = 0; j < kalimat.length(); j++) {
				if (kalimat.charAt(j) == huruf.charAt(i)) {
					hasil += huruf.charAt(i);
					break;
				}
			}
		}

		if (hasil.equals(huruf)) {
			System.out.println("panagram");
		} else {
			System.out.println("not panagram");
		}

		if (hasil.length() == 0) {
			System.out.println("Huruf di luar Range");
		}

	}

	private static void soal7() {

	}

	private static void soal8() {
		System.out.print("Masukan Yang Jumlah Ulang: : ");
		int jumlah = in.nextInt();

		String[] sampel = new String[jumlah];

		for (int i = 0; i < jumlah; i++) {
			System.out.println("Silahkan Kalimat ke-" + (i + 1) + ": ");
			sampel[i] = in.next();
		}

		String huruf = "abc";
		int g = 0;

		for (int i = 0; i < jumlah; i++) {

			String hasil = "";

			for (int j = 0; j < huruf.length(); j++) {
				for (int k = 0; k < sampel[i].length(); k++) {
					if (sampel[i].charAt(k) == huruf.charAt(j)) {
						hasil += huruf.charAt(j);
						break;
					}
				}
			}

			if (hasil.equals(huruf)) {
				g++;
			}

		}
		System.out.println(g);

	}

	private static void soal9() {
		int k = 0;
		int p = 0;
		String huruf, huruf2;
		System.out.print("Masukan Huruf: ");
		huruf = in.nextLine();
		System.out.print("Masukan Huruf: ");
		huruf2 = in.nextLine();
		char[] ubah = huruf.toCharArray();
		char[] ubah2 = huruf2.toCharArray();

		for (int i = 0; i < ubah.length; i++) {
			if (ubah[i] != 'c')
				k++;
		}
		for (int i = 0; i < ubah2.length; i++) {
			if (ubah2[i] != 'c')
				p++;
		}
		k = k + p;
		System.out.println(k);
	}

	private static void soal10() {
		System.out.print("Silahkan masukkan jumlah pasangan: ");
		int jumlah = in.nextInt();
		in.nextLine();
		String[][] pasangan = new String[jumlah][2];

		for (int i = 0; i < jumlah; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.print("Silahkan masukkan anggota pasangan ke-" + (i + 1) + " kata ke-" + (j + 1) + ": ");
				pasangan[i][j] = in.next().toLowerCase();
			}
		}
		for (int i = 0; i < jumlah; i++) {

			int p = 0;
			for (int j = 0; j < pasangan[i][0].length(); j++) {
				for (int k = 0; k < pasangan[i][1].length(); k++) {
					if (pasangan[0][0].charAt(j) == pasangan[0][1].charAt(k)) {
						p++;
						break;
					}
				}
			}
			if (p > 0) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}

		}

	}
}
