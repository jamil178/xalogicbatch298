package day6;

import java.util.Scanner;

public class soalbaru {
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {

				System.out.print("Masukan Soal: ");
				int number = in.nextInt();
				in.nextLine();
				switch (number) {
				case 1:
					soal1();

					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();

					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();

					break;
				case 6:
					soal6();

					break;
				case 7:
					soal7();

					break;
				default:
					System.out.println("Sorry Gak Ada Yang DI Pilih");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Mau Masukan Soal?");
			answer = in.next();
		}

	}

	private static void soal1() {
		int n1 = 0;
		int n2 = 1;
		int n3 = 0;
		int i = 0;
		int nilai;
		System.out.print("Masukan nilai: ");
		nilai = in.nextInt();
		System.out.print("" + n2);

		for (i = 1; i <= nilai; ++i) {
			n3 = n1 + n2;
			System.out.print(" " + n3);
			n1 = n2;
			n2 = n3;

		}

	}

	private static void soal2() {
		System.out.print("Masukan Nilai: ");
		int nilai = in.nextInt();
		int[] inputanangka = new int[nilai];
		for (int i = 0; i < inputanangka.length; i++) {
			if (i > 2) {
				inputanangka[i] = inputanangka[i - 1] + inputanangka[i - 2] + inputanangka[i - 3];

			} else {

				inputanangka[i] = 1;
			}
			System.out.print(inputanangka[i] + " ");
		}

	}

	private static void soal3() {
		System.out.print("Masukan nilai: ");
		int nilai = in.nextInt();

		for (int i = 2; i < nilai; i++) {
			int k = 1;
			for (int p = 2; p < i; p++) {
				if (i % p == 0) {
					k = 0;
				}
			}
			if (k == 1) {
				System.out.print(i + " ");
			}
		}
		System.out.print("");

	}

	private static void soal4() {
		System.out.print("Inputkan Jam : ");
		String inputan = in.nextLine().toLowerCase();
		System.out.print("Masukan Situasi: ");
		String situasi = in.nextLine().toLowerCase();
		String[] arrayInputan = inputan.split(":");

		int k = Integer.parseInt(arrayInputan[0]);
		String i = arrayInputan[1];
		String p = arrayInputan[2];
		if (k > 12) {
			System.out.println("Nilai Jam Kelebihan");
		}
		if (situasi.equals("pm") || situasi.equals("PM")) {
			arrayInputan[2] = Integer.toString(k);
			k = k + 12;
			System.out.println(k + ":" + i + ":" + p);
		}
		if (situasi.equals("am") || situasi.equals("AM")) {
			arrayInputan[2] = Integer.toString(k);
			System.out.println(k + ":" + i + ":" + p);
		}
	}

	private static void soal5() {
		System.out.print("Masukan nilai: ");// masukan nilai yang bukan bilangan prima
		int nilai = in.nextInt();
		int prima = 2;
		int result = 0;
		while (nilai != 1) {
			result = nilai;
			if (result % prima == 0) {
				result = nilai / prima;
				System.out.println("" + nilai + "/" + prima + "=" + result);
				nilai = result;
			} else {
				prima++;
			}

		}
	}

	private static void soal6() {
		System.out.print("Masukkan indeks kostemer: ");
		String inputKostemer = in.nextLine();
		int[] arrayJarakKostemer = { 2000, 500, 1500, 300 };
		int bensin = 2500;
		String tempString = "";
		int tempInt = 0;
		String[] arrayInputKostemer = inputKostemer.split(" ");
		if (arrayInputKostemer.length > arrayJarakKostemer.length) {
			System.out.println("Input melebihi database");
			return;
		}
		if (!arrayInputKostemer[0].equals("1")) {
			System.out.println("Maaf orderan salah");
			return;
		}
		for (int i = 0; i < arrayInputKostemer.length; i++) {
			if (Integer.parseInt(arrayInputKostemer[i]) > tempInt) {
				tempInt = Integer.parseInt(arrayInputKostemer[i]);
			}
		}
		if (tempInt > arrayJarakKostemer.length) {
			System.out.println("Indeks maksimal input melebihi database");
			return;
		}
		for (int i = 0; i < tempInt; i++) {
			tempString += (i + 1) + "";
			if (i != tempInt - 1) { // tambah spasi
				tempString += " ";
			}
		}
		arrayInputKostemer = tempString.split(" ");
		tempString = "";
		tempInt = 0;
		for (int i = 0; i < arrayInputKostemer.length; i++) {
			int kostemer = Integer.parseInt(arrayInputKostemer[i]) - 1;

			String jarakKostemer = "";
			if (arrayJarakKostemer[kostemer] <= 1000) {
				jarakKostemer = arrayJarakKostemer[kostemer] + "M";
			} else {
				jarakKostemer = (arrayJarakKostemer[kostemer] / 1000.0) + "KM";
			}

			if (i == 0) {
				tempString += "Jarak tempuh = " + jarakKostemer + " + ";
				tempInt += arrayJarakKostemer[kostemer];
			} else if (i == arrayInputKostemer.length - 1) {
				tempString += jarakKostemer;
				tempInt += arrayJarakKostemer[kostemer];
			} else {
				tempString += jarakKostemer + " + ";
				tempInt += arrayJarakKostemer[kostemer];
			}
		}
		// perhitungan hasil jarak
		if (tempInt <= 1000) {
			tempString += " = " + tempInt + "M" + "\n";
		} else {
			tempString += " = " + (tempInt / 1000.0) + "KM" + "\n";
		}
		tempInt /= bensin;
		if (tempInt % bensin != 0) {
			tempInt++;
		}
		tempString += "Bensin = " + tempInt + " Liter";
		tempString = tempString.replaceAll(".0K", "K");

		System.out.println(tempString);

	}

	private static void soal7() {
		System.out.print("Masukkan SOS Anda: ");
		String sos = in.nextLine();
		char[] arrayCharSos = sos.toLowerCase().toCharArray();
		int k = 0;
		for (int i = 0; i < arrayCharSos.length; i++) {
			if (i % 3 == 0 && arrayCharSos[i] == 's') {
				continue;
			} else if (i % 3 == 1 && arrayCharSos[i] == 'o') {
				continue;
			} else if (i % 3 == 2 && arrayCharSos[i] == 's') {
				continue;
			} else {
				k++;
			}
		}

		System.out.println(k);
	}
}
