package day7;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";
		while (answer.toUpperCase().equals("Y")) {
			try {

				System.out.print("Masukan Soal: ");
				int number = in.nextInt();
				in.nextLine();
				switch (number) {
				case 3:
					soal3();

					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();

					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();

					break;
				case 8:
					soal8();

					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();

					break;
				default:
					System.out.println("Sorry Gak Ada Yang DI Pilih");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Mau Masukan Soal?");
			answer = in.next();
		}

	}

	private static void soal3() {
		System.out.println("-------Nomor 3------ ");
		System.out.print("Masukan Jumlah: ");
		int jumlah = in.nextInt();
		int result = 0;
		for (int i = 1; i <= jumlah; i++) {
			System.out.print("Masukan Nilai: ");
			int nilai = in.nextInt();
			result = nilai + result;

		}
		System.out.println(result);

	}

	private static void soal4() {
		System.out.println("-------soal 4 ----------");
		System.out.print("Masukkan n matrix persegi: ");
		int n = in.nextInt();
		int [][] array = new int [n][n];
		for(int baris=0; baris<n; baris++ ) {
			for(int kolom=0; kolom<n; kolom++ ) {
				System.out.print("Masukan Elemen Matriks["+baris+"]["+kolom+"]: ");
				array[baris][kolom]=in.nextInt();
			}
		}
		int d1=0;
		for(int i=0; i<n; i++ ) {
			d1+=array[i][i];
		}
		int d2=0;
		for(int k=0; k<n; k++ ) {
			System.out.println(n);
			d2+=array[k][(n-1)-k];
			
		}
		System.out.println("nilai 1= "+d1);
		System.out.println("nilai 2= "+d2);
		int l=d2-d1;
		if(l<=0) {
			l=l*(-1);
		System.out.println(l);
		}
		else {
		System.out.println(l);
		}
	}

	private static void soal5() {
		System.out.println("Masukkan n: ");
		int n = in.nextInt();
		in.nextLine();
		System.out.println("Masukkan array: ");
		String string = in.nextLine();

		String[] arrayString = string.split(" ");

		int negative = 0;
		int zero = 0;
		int positive = 0;

		for (int i = 0; i < arrayString.length; i++) {
			double value = Integer.parseInt(arrayString[i]);
			if (value < 0) {
				negative++;
			} else if (value == 0) {
				zero++;
			} else {
				positive++;
			}
		}
		
		DecimalFormat decimalFormat = new DecimalFormat("0.000000");
		string = decimalFormat.format((float) (positive) / n) + "\n" + decimalFormat.format((float) (negative) / n) + "\n" + decimalFormat.format ((float) (zero) / n);
		System.out.println(string);

	}

	private static void soal6() {
		System.out.println("-------Nomor 6------ ");
		System.out.print("Masukan Nilai: ");
		int p = in.nextInt();
		for (int i = 0; i < p; i++) {

			for (int j = 0; j < p; j++) {

				if (i + j > p - 2) {
					System.out.print("#");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}

	}

	private static void soal7() {
		System.out.println("Masukkan array: ");
		String string = in.nextLine();
		String[] arrayString = string.split(" ");

		int max = -99999999;
		int min = 99999999;
		for (int i = 0; i < arrayString.length; i++) {
			int sum = 0;
			for (int j = 0; j < arrayString.length; j++) {
				if (i != j) {
					sum += Integer.parseInt(arrayString[j]);
//				System.out.print(sum+" ");
				}
			}
			if (max < sum) {
				max = sum;
			}
			if (min > sum) {
				min = sum;
			}
		}

		System.out.println(max + " " + min);
	}

	private static void soal8() {
		System.out.println("-------Nomor 8------ ");
		System.out.print("Masukan Jumlah : ");
		int n = in.nextInt();
		int nilaimax = 0;
		int tambah = 0;
		int k = 0;
		for (int i = 0; i < n; i++) {
			System.out.print("Masukan Angka : ");
			k = in.nextInt();
			if (k > nilaimax) {
				tambah = 1;
				nilaimax = k;
			} else if (k == nilaimax) {
				tambah++;
			}
		}
		System.out.println(tambah);
	}

	private static void soal9() {
		System.out.println("-------Nomor 9------ ");
		System.out.print("Masukan Jumlah: ");
		long jumlah = in.nextInt();
		long result = 0;
		for (long i = 1; i <= jumlah; i++) {
			System.out.print("Masukan Nilai: ");
			long nilai = in.nextInt();
			result = nilai + result;

		}
		System.out.println(result);
	}

	private static void soal10() {
		System.out.println("-------Nomor 10------ ");
		System.out.print("Masukan Jumlah : ");
		int n = in.nextInt();
		int nilaimax = 0;
		int tambah = 0;
		int k = 0;
		for (int i = 0; i < n; i++) {
			System.out.print("Masukan Angka : ");
			k = in.nextInt();
			if (k > nilaimax) {
				tambah = 1;
				nilaimax = k;
			} else if (k == nilaimax) {
				tambah++;
			}
		}
		System.out.println(tambah);

	}
}