package day4;

import java.util.Scanner;

public class TwoDimenNomor8 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		int[][] arrays = new int[3][n];
		int temp = 0;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i % 3 == 0) {
					temp = j;
					arrays[i][j] = temp;
				} else if (i % 3 == 1) {
					arrays[i][j] = temp;
					temp += 2;
				} else {
					temp = arrays[i - 1][j] + arrays[i - 2][j];
					arrays[i][j] = temp;
					
				}

				System.out.print(arrays[i][j] + " ");
			}
			temp = 0;
			System.out.println();
		}
	}

}
