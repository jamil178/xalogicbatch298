package day4;

import java.util.Scanner;

public class TwoDimenNomor9 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		int[][] arrays = new int[3][n];

		int temp = 0;
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if(i%3==0) {
					arrays[i][j] = j;
				} else if(i%3==1) {
					arrays[i][j] = temp;
					temp += 3;
				} else if(i%3==2) {
					temp -= 3;
					arrays[i][j] = temp;
				}
//				arrays[i][j] = 0;
				System.out.print(arrays[i][j]+" ");
			}
			System.out.println();
		}
	}

}
