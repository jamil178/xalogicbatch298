package day4;

import java.util.Scanner;

public class TwoDimenNomor4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2: ");
		int n2 = input.nextInt();
		int[][] arrays = new int[2][n];
		int temp = 0;
		int temp1 = 0;
		int temp2 = n2;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {

				if (i % 2 == 0) {
					temp = j;
					arrays[i][j] = temp;
				} else {
					if (j % 2 == 0) {
						temp1++;
						arrays[i][j] = temp1;
					} else {

						arrays[i][j] = temp2;
						temp2 += n2;
					}
				}

				System.out.print(arrays[i][j] + " ");
			}
			temp1 = 0;
			temp2 = n2;
			System.out.println();
		}
	}

}
