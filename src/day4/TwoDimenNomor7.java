package day4;

import java.util.Scanner;

public class TwoDimenNomor7 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		int[][] arrays = new int[3][n];
		int temp = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i % 3 == 0) {
					arrays[i][j] = j;
				} else if(i%3 ==1) {
					temp = n+j;
					arrays[i][j] = temp;
				} else if(i%3==2) {
					temp = n*2+j;
					arrays[i][j] = temp;
				}
				System.out.print(arrays[i][j]+" ");
			}
			System.out.println();

		}

	}

}
