package day4;

import java.util.Scanner;

public class TwoDimenNomor10 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2: ");
		int n2 = input.nextInt();
		int[][] arrays = new int[n2][n];
		int temp = 0;
		
		for(int i=0;i<n2;i++) {
			for(int j=0;j<n;j++) {
				if(i%3==0) {
					arrays[i][j] = j;
				} else if(i%3==1) {
					arrays[i][j] = temp;
					temp +=3;
				} else {
					arrays[i][j] = temp;
					temp += 4;
				}
//				arrays[i][j] = 0;
				System.out.print(arrays[i][j]+" ");
			}
			temp = 0;
			System.out.println();
		}

	}

}
