package day4;

import java.util.Scanner;

public class TwoDimenNomor6 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n: ");
		int n = input.nextInt();
		int[][] arrays = new int[3][n];
		int temp = 1;
		
		for(int i=0;i<3;i++) {
			for(int j=0;j<n;j++) {
				if(i%3==0) {
					arrays[i][j] = j;
				} else if(i%3==1) {
					arrays[i][j] = temp;
					temp *= n;
				} else {
					arrays[i][j] = arrays[i-1][j] + arrays[i-2][j];
				}
				
				System.out.print(arrays[i][j]+" ");
			}
			temp = 1;
			System.out.println();
		}

	}

}
